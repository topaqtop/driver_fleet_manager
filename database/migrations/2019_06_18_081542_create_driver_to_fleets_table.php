<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverToFleetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_to_fleets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('fleet_id');
            $table->integer('driver_id');
            $table->dateTime('date_from');
            $table->dateTime('date_to');
            $table->integer('employee_conveyed_id');
            $table->integer('approved_by');
            $table->integer('transaction_status');
            $table->date('request_date')->nullable();
            $table->date('approve_date')->nullable();
            $table->date('reject_date')->nullable();
            $table->date('completed_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_to_fleets');
    }
}
