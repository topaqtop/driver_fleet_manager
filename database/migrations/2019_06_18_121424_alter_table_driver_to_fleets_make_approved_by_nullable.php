<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDriverToFleetsMakeApprovedByNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('driver_to_fleets', function (Blueprint $table) {
            //
            $table->integer('approved_by')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_to_fleets', function (Blueprint $table) {
            //
            $table->integer('approved_by')->change();
        });
    }
}
