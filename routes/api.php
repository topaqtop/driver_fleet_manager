<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['api']], function () {
    // your routes here
    Route::get('/get_role_permissions', 'RolePermissionController@getRolePermissions');
    Route::post('/employee_login', 'EmployeeController@employeeLogin');
    Route::get('/employee_logout', 'EmployeeController@employeeLogout');
    Route::post('/get_permissions_by_role', 'RolePermissionController@getPermissionsByRole');
    Route::get('/get_all_emplyees', 'EmployeeController@viewEmployee');
    Route::post('/get_employee_by_id', 'EmployeeController@getEmployeesById');   
    Route::post('/create_employee', 'EmployeeController@addEmployee');
    Route::put('/edit_employee', 'EmployeeController@editEmployee');
    Route::delete('/delete_employee', 'EmployeeController@deleteEmployee');
    Route::put('/assign_role', 'RolePermissionController@assignRole');


    Route::post('/create_vehicle', 'VehicleController@createVehicle');
    Route::put('/edit_vehicle', 'VehicleController@editVehicle');
    Route::get('/get_vehicles', 'VehicleController@getVehicles');
    Route::post('/get_vehicle_by_id', 'VehicleController@getVehicleById');
    Route::delete('/delete_vehicle', 'VehicleController@deleteVehicle');

    Route::post('/create_vehicle_request', 'RequestController@driverRequestAction');
    Route::put('/approve_vehicle_request', 'RequestController@approverApproveRequest');
    Route::put('/complete_vehicle_request', 'RequestController@driverCompleteRequest');
});

// Route::get('/test_route',function(){
//     return "ok"; 
// });