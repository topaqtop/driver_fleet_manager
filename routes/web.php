<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', function () {

    return view('index');
});

Route::get('/create_employee', function () {

    return view('create_employee');
});
Route::get('/view_employees', function () {

    return view('view_employees');
});

Route::get('/edit_employee', function () {

    return view('edit_employee');
});

Route::get('/create_vehicle', function () {

    return view('create_vehicle');
});

Route::get('/get_vehicles', function () {

    return view('get_vehicles');
});

Route::get('/edit_vehicle', function () {

    return view('edit_vehicle');
});
