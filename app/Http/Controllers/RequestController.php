<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use App\driver_to_fleet;
use Carbon\Carbon;

class RequestController extends BaseController
{
    //use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function driverRequestAction(Request $request)
    {
        try {

            if (!$this->authorize($request->header('Authorization'), $request)) {
                return ['status' => 'error', 'message' => 'Unauthorized user', 'data' => []];
            }

            $validator = Validator::make($request->all(), [
                'fleet_id' => 'required',
                'driver_id' => 'required',
                'date_from' => 'required',
                'date_to' => 'required',
                'employee_conveyed_id' => 'required',
                'transaction_status' => 'required'
            ]);

            if ($validator->fails()) {
                return ['status' => 'error', 'message' => $validator->errors()];
            }

            $data = [
                'fleet_id' => $request->fleet_id,
                'driver_id' => $request->driver_id,
                'date_from' => $request->date_from,
                'date_to' => $request->date_to,
                'employee_conveyed_id' => $request->employee_conveyed_id,
                'transaction_status' => 2,
                'request_date' => Carbon::now()
            ];

            $checkInUse = driver_to_fleet::where('driver_id', $request->driver_id)
            ->where('fleet_id', $request->fleet_id)
            ->where('completed_date', null)
            ->first();

            if($checkInUse){
                return [
                    'status' => 'error',
                    'message' => 'Vehicle in use',
                    'data' => $checkInUse
                ];
            }

            $checkData = driver_to_fleet::insert(
                    $data
                );

            return [
                'status' => 'success',
                'message' => 'successful',
                'data' => $checkData
            ];
        } catch (\Exception $error) {
            return ['status' => 'error', 'message' => $error];
        }
    }

    public function approverApproveRequest(Request $request)
    {
        try {

            if (!$this->authorize($request->header('Authorization'), $request)) {
                return ['status' => 'error', 'message' => 'Unauthorized user', 'data' => []];
            }

            $validator = Validator::make($request->all(), [
                'request_id' => 'required',
                'approved_by' => 'required',
                'approve_action' => 'required'
            ]);

            if ($validator->fails()) {
                return ['status' => 'error', 'message' => $validator->errors()];
            }

            $data = [
                'approve_date' => $request->approve_action == 1 ? Carbon::now() : null,
                'reject_date' => $request->approve_action == 1 ? null : Carbon::now(),
                'approved_by' => $request->approved_by,
                'transaction_status' => $request->approve_action == 1 ? 3 : 4,
            ];

            $checkData = driver_to_fleet::where('id', $request->request_id)
                ->update(
                    $data
                );

            return [
                'status' => 'success',
                'message' => 'successful',
                'data' => $checkData
            ];
        } catch (\Exception $error) {
            return ['status' => 'error', 'message' => $error];
        }
    }

    public function driverCompleteRequest(Request $request)
    {
        try {

            if (!$this->authorize($request->header('Authorization'), $request)) {
                return ['status' => 'error', 'message' => 'Unauthorized user', 'data' => []];
            }

            $validator = Validator::make($request->all(), [
                'request_id' => 'required'
            ]);

            if ($validator->fails()) {
                return ['status' => 'error', 'message' => $validator->errors()];
            }

            $data = [
                'completed_date' => Carbon::now()
            ];

            $checkData = driver_to_fleet::where('id', $request->request_id)
                ->update(
                    $data
                );

            return [
                'status' => 'success',
                'message' => 'successful',
                'data' => $checkData
            ];
        } catch (\Exception $error) {
            return ['status' => 'error', 'message' => $error];
        }
    }

    public function authorize($token, $request)
    {
        if ($token != env('APP_KEY')) {
            return false;
        }
        return true;
    }
}
