<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use App\employee_role;
use App\User;

class RolePermissionController extends BaseController
{
    //use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getRolePermissions()
    {

        // if (!$this->authorize($request->header('Authorization'), $request)) {
        //     return ['status' => 'error', 'message' => 'Unauthorized user', 'data' => []];
        // }

        $rolePermissions = [
            '1' => [
                'get_vehicles',
                'view_reports',
                'view_employees'
            ],
            '2' => [
                'get_vehicles',
                'view_reports',
                'view_employees',
                'approve_vehicle_request'
            ],
            '3' => [
                'get_vehicles',
                'view_reports',
                'view_employees',
                'approve_vehicle_request',
                'create_vehicle',
                'edit_vehicle',
                'delete_vehicle',
                'create_employee',
                'edit_employee',
                'delete_employee',
                'assign_role'
            ]
        ];

        return ['status' => 'success', 'message' => 'role_permissions fetched successfully', 'data' => $rolePermissions];
    }

    public function getPermissionsByRole(Request $request)
    {

        // if (!$this->authorize($request->header('Authorization'), $request)) {
        //     return ['status' => 'error', 'message' => 'Unauthorized user', 'data' => []];
        // }

        $rolePermissions = $this->getRolePermissions();
        if ($request->role != 1 && $request->role != 2 && $request->role != 3) {
            return ['status' => 'error', 'message' => 'role is either driver, approver or manager', 'data' => []];
        }
        return ['status' => 'success', 'message' => 'data fetched successfully', 'data' => $rolePermissions['data'][$request->role]];
    }

    public function assignRole(Request $request)
    {
        try {

            if (!$this->authorize($request->header('Authorization'), $request)) {
                return ['status' => 'error', 'message' => 'Unauthorized user', 'data' => []];
            }

            $validator = Validator::make($request->all(), [
                'employee_id' => 'required',
                'role_id' => 'required'
            ]);

            if ($validator->fails()) {
                return ['status' => 'error', 'message' => $validator->errors()];
            }

            $checkEmployee = User::where('employee_id', $request->employee_id)
                ->first();

            if (!$checkEmployee) {
                return ['status' => 'error', 'message' => 'Employee id not found'];
            }

            $checkRole = employee_roles::where('id', $request->role_id)
                ->first();

            if (!$checkRole) {
                return ['status' => 'error', 'message' => 'Role id not found'];
            }

            $checkData = User::where('employee_id', $request->employee_id)
                ->update(
                    ['role_id' => $request->role_id]
                );

                return ['status' => 'success', 'message' => 'Employee role update successfully', 'data' => $checkData];

        } catch (\Exception $error) {
            return ['status' => 'error', 'message' => $error];
        }
    }

    public function authorize($token, $request)
    {
        if ($token != env('APP_KEY')) {
            return false;
        }
        return true;
    }
}
