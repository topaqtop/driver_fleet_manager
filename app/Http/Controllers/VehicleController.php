<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use App\fleets;


class VehicleController extends BaseController
{
    //use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getVehicles(Request $request){
        try{
            if(!$this->authorize($request->header('Authorization'), $request))
            {
                return ['status' => 'error', 'message' => 'Unauthorized user', 'data' => []];
            }
            $checkData = fleets::get();

            if($checkData){
                
                return [
                    'status' => 'success',
                    'message' => 'successful',
                    'data' => $checkData
                ];

            }
            else{
                return ['status' => 'error', 'message' => 'failed', 'data' => []];
            }

        }catch(\Exception $error){
            return ['status' => 'error', 'message' => $error];
        }
    }

    public function getVehicleById(Request $request){
        try{
            if(!$this->authorize($request->header('Authorization'), $request))
            {
                return ['status' => 'error', 'message' => 'Unauthorized user', 'data' => []];
            }

            $validator = Validator::make($request->all(), [
                'fleet_id' => 'required'
            ]);
    
            if ($validator->fails()) {
                return ['status' => 'error', 'message' => $validator->errors()];
            }

            $checkData = fleets::where('fleet_id', $request->fleet_id)
            ->first();

            if($checkData){
                
                return [
                    'status' => 'success',
                    'message' => 'successful',
                    'data' => $checkData
                ];

            }
            else{
                return ['status' => 'error', 'message' => 'data not found', 'data' => []];
            }

        }catch(\Exception $error){
            return ['status' => 'error', 'message' => $error];
        }
    }

    public function createVehicle(Request $request){
        try{
            if(!$this->authorize($request->header('Authorization'), $request))
            {
                return ['status' => 'error', 'message' => 'Unauthorized user', 'data' => []];
            }

            $validator = Validator::make($request->all(), [
                'fleet_name' => 'required'
            ]);
    
            if ($validator->fails()) {
                return ['status' => 'error', 'message' => $validator->errors()];
            }

            $data = [
                'fleet_id' => mt_rand(1000000, 9999999),
                'fleet_name' => $request->fleet_name
            ];

            $checkData = fleets::insert(
                $data
            );

                return [
                    'status' => 'success',
                    'message' => 'successful',
                    'data' => $data
                ];

        }catch(\Exception $error){
            return ['status' => 'error', 'message' => $error];
        }
    }

    public function editVehicle(Request $request){
        try{
            if(!$this->authorize($request->header('Authorization'), $request))
            {
                return ['status' => 'error', 'message' => 'Unauthorized user', 'data' => []];
            }

            $validator = Validator::make($request->all(), [
                'fleet_name' => 'required',
                'fleet_id' => 'required'
            ]);
    
            if ($validator->fails()) {
                return ['status' => 'error', 'message' => $validator->errors()];
            }

            $data = [
                'fleet_name' => $request->fleet_name
            ];

            $checkData = fleets::where('fleet_id', $request->fleet_id)
            ->update(
                $data
            );

                return [
                    'status' => 'success',
                    'message' => 'successful',
                    'data' => $data
                ];

        }catch(\Exception $error){
            return ['status' => 'error', 'message' => $error];
        }
    }

    public function deleteVehicle(Request $request){
        try{
            if(!$this->authorize($request->header('Authorization'), $request))
            {
                return ['status' => 'error', 'message' => 'Unauthorized user', 'data' => []];
            }

            $validator = Validator::make($request->all(), [
                'fleet_id' => 'required'
            ]);
    
            if ($validator->fails()) {
                return ['status' => 'error', 'message' => $validator->errors()];
            }

            $checkData = fleets::where('fleet_id', $request->fleet_id)
            ->first();

            if($checkData){
            $deleteData = fleets::where('fleet_id', $request->fleet_id)
            ->delete();

                return [
                    'status' => 'success',
                    'message' => 'successful',
                    'data' => $checkData
                ];
            }else{
                return ['status' => 'error', 'message' => 'data not found', 'data' => []];
            }


        }catch(\Exception $error){
            return ['status' => 'error', 'message' => $error];
        }

    }

    public function authorize($token, $request){
        if($token != env('APP_KEY'))
        {
            return false;
        }
        return true;
    }
}
