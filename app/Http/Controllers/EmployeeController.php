<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\RolePermissionController;
use DB;
use Auth;
use App\User;
use Illuminate\Contracts\Auth\Authenticatable;


class EmployeeController extends BaseController
{
    //use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function employeeLogin(Request $request){
        try{

            $rolePermissions = new RolePermissionController();
            $validator = Validator::make($request->all(), [
                'username' => 'required',
                'password' => 'required',
            ]);
    
            if ($validator->fails()) {
                return ['status' => 'error', 'message' => $validator->errors()];
            }

            $checkData = User::where('employee_name', $request->username)
            ->where('employee_id', $request->password)
            ->first();

            if($checkData){

                $role = $rolePermissions->getRolePermissions()['data'][$checkData->role_id];
                
                $checkData->roles = $role;
                
                //Auth::login($checkData);

                return [
                    'status' => 'success',
                    'message' => 'Login successful',
                    'data' => $checkData
                ];

            }
            
            else{
                return ['status' => 'error', 'message' => 'Login failed', 'data' => []];
            }

        }catch(\Exception $error){
            return ['status' => 'error', 'message' => $error];
        }
    }

    public function viewEmployee(Request $request){
        try{
            if(!$this->authorize($request->header('Authorization'), $request))
            {
                return ['status' => 'error', 'message' => 'Unauthorized user', 'data' => []];
            }
            $checkData = User::get();

            if($checkData){
                
                return [
                    'status' => 'success',
                    'message' => 'successful',
                    'data' => $checkData
                ];

            }
            else{
                return ['status' => 'error', 'message' => 'failed', 'data' => []];
            }

        }catch(\Exception $error){
            return ['status' => 'error', 'message' => $error];
        }
    }

    public function getEmployeesById(Request $request){
        try{
            if(!$this->authorize($request->header('Authorization'), $request))
            {
                return ['status' => 'error', 'message' => 'Unauthorized user', 'data' => []];
            }

            $validator = Validator::make($request->all(), [
                'employee_id' => 'required'
            ]);
    
            if ($validator->fails()) {
                return ['status' => 'error', 'message' => $validator->errors()];
            }

            $checkData = User::where('employee_id', $request->employee_id)
            ->first();

            if($checkData){
                
                return [
                    'status' => 'success',
                    'message' => 'successful',
                    'data' => $checkData
                ];

            }
            else{
                return ['status' => 'error', 'message' => 'data not found', 'data' => []];
            }

        }catch(\Exception $error){
            return ['status' => 'error', 'message' => $error];
        }
    }

    public function addEmployee(Request $request){
        try{
            if(!$this->authorize($request->header('Authorization'), $request))
            {
                return ['status' => 'error', 'message' => 'Unauthorized user', 'data' => []];
            }

            $validator = Validator::make($request->all(), [
                'employee_name' => 'required',
                'role_id' => 'required',
                'employment_date' => 'required',
            ]);
    
            if ($validator->fails()) {
                return ['status' => 'error', 'message' => $validator->errors()];
            }

            $data = [
                'employee_id' => mt_rand(1000000, 9999999),
                'employee_name' => $request->employee_name,
                'role_id' => $request->role_id,
                'employment_date' => $request->employment_date,
            ];

            $checkData = User::insert(
                $data
            );

                return [
                    'status' => 'success',
                    'message' => 'successful',
                    'data' => $data
                ];

        }catch(\Exception $error){
            return ['status' => 'error', 'message' => $error];
        }
    }

    public function editEmployee(Request $request){
        try{
            if(!$this->authorize($request->header('Authorization'), $request))
            {
                return ['status' => 'error', 'message' => 'Unauthorized user', 'data' => []];
            }

            $validator = Validator::make($request->all(), [
                'employee_id' => 'required',
                'employee_name' => 'required',
                'employee_date' => 'required',
            ]);
    
            if ($validator->fails()) {
                return ['status' => 'error', 'message' => $validator->errors()];
            }

            $data = [
                'employee_name' => $request->employee_name,
                'employment_date' => $request->employee_date,
            ];

            $checkData = User::where('employee_id', $request->employee_id)
            ->update(
                $data
            );

                return [
                    'status' => 'success',
                    'message' => 'successful',
                    'data' => $data
                ];

        }catch(\Exception $error){
            return ['status' => 'error', 'message' => $error];
        }
    }

    public function deleteEmployee(Request $request){
        try{
            if(!$this->authorize($request->header('Authorization'), $request))
            {
                return ['status' => 'error', 'message' => 'Unauthorized user', 'data' => []];
            }

            $validator = Validator::make($request->all(), [
                'employee_id' => 'required'
            ]);
    
            if ($validator->fails()) {
                return ['status' => 'error', 'message' => $validator->errors()];
            }

            $checkData = User::where('employee_id', $request->employee_id)
            ->first();

            if($checkData){
            $deleteData = User::where('employee_id', $request->employee_id)
            ->delete();

                return [
                    'status' => 'success',
                    'message' => 'successful',
                    'data' => $checkData
                ];
            }else{
                return ['status' => 'error', 'message' => 'data not found', 'data' => []];
            }

        }catch(\Exception $error){
            return ['status' => 'error', 'message' => $error];
        }

    }

    public function authorize($token, $request){
        if($token != env('APP_KEY'))
        {
            return false;
        }
        return true;
    }
}
