$(function(){
    

});

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

function checkPermission() {
    var empData = JSON.parse(localStorage.getItem('user'));
    if (isEmpty(empData)) {
        window.location.href = '/login';
    }

    var currentPath = window.location.pathname;
    var lastslashindex = currentPath.lastIndexOf('/');
    var currentRole = currentPath.substring(lastslashindex  + 1);
    if(jQuery.inArray(currentRole, empData.data.roles) === -1){
        window.location.href = '/login';
    }
}

function signOut(){
    localStorage.clear();
    window.location.href = '/login';
}

function signIn() {
    $.ajax({
        type: "POST",
        dataType: 'json',
        data: {
            "username": $('#username').val(),
            "password": $('#password').val()
        },
        url: "http://localhost:8001/api/employee_login",
        success: function (data) {
            //console.log(data);
            if (data.status == "success") {
                alertify.success(data.message);

                
                localStorage.setItem('user', JSON.stringify(data));
                //console.log('mmm', JSON.parse(localStorage.getItem('user')))
                console.log(data.data);

                window.location.href = '/view_employees';

            }
            else {
                if (typeof data.message == "string") {
                    alertify.error(data.message);
                }
                else {
                    console.log(data.message);
                    Object.values(data.message).map(function (key, index) {
                        console.log(key[0]);
                        alertify.error(key[0]);
                    });

                }

            }

        },
        error: function (error) {
            alertify.error(error.message);
        }
    });
}

function myFunction() {

    var empData = JSON.parse(localStorage.getItem('user'));
    var empRole = empData.data.roles;
    //console.log('llllllllll', empRole)
    var menus = "";
    var count = 0;
    for (var role in empRole) {
        var sideMenu = empRole[count].replace(/\_/g, ' ');
        //console.log('uuuuuuuu', sideMenu)
        if(sideMenu.indexOf('edit') === -1 && sideMenu.indexOf('delete') === -1){
        menus = menus + '<li>' +
            '<a href="/' + empRole[count] + '" class="myclass">' +
            '<i class="fas fa-map-marker-alt"></i>' + sideMenu + '</a>' +
        '</li>';
        
        }
        count++;
    }

    console.log('eeee', menus)
    $('.menu-sidebar__content').html('<nav class="navbar-sidebar"><ul class="list-unstyled navbar__list">' + menus + '</ul></nav>');
    $('.navbar-mobile').html('<div class="container-fluid"><ul class="navbar-mobile__list list-unstyled" >' + menus + '</div></ul>');

    //document.getElementByClassName("menu-sidebar__content").innerHTML = '<nav class="navbar-sidebar"><ul class="list-unstyled navbar__list">'+menus+'</ul></nav>';
    //document.getElementByClassName("navbar-mobile").innerHTML = '<div class="container-fluid"><ul class="navbar-mobile__list list-unstyled" >'+menus+'</div></ul>';
}